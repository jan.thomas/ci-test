#!/usr/bin/env bash
set -euo pipefail

flatpak install org.kde.Platform/x86_64/5.15-21.08
flatpak install org.kde.Sdk/x86_64/5.15-21.08

flatpak-builder --force-clean --repo=repo build org.manymoney.citest.yaml
flatpak build-bundle ./repo citest.flatpak org.manymoney.citest