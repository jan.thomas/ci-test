cmake_minimum_required(VERSION 3.22)
project(citest)

set(CMAKE_CXX_STANDARD 20)

add_executable(citest main.cpp)

install(TARGETS citest)
